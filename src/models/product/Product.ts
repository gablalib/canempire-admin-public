import { ProductFormat } from './ProductFormat'

export interface Product {
  id?: string
  name?: string
  brand?: string
  producer?: string
  description?: string
  type?: string
  category?: string
  dominance?: string
  intensity?: string
  minThc?: number
  maxThc?: number
  thcUnit?: string
  minCbd?: number
  maxCbd?: number
  cbdUnit?: string
  formats?: ProductFormat[]
}

export const ProductType = [
  '',
  'Fleurs séchées',
  'Moulu',
  'Préroulés',
  'Atomisateurs oraux',
  'Capsules',
  'Concentrés',
  'Hashish',
  'Huiles'
  ]

export const ProductCategory = [
  '',
  'Indica',
  'Sativa',
  'Hybride',
  'Mélange',
  'Dérivés'
]

export const ProductDominance = [
  '',
  'THC',
  'CBD',
  'Équilibré'
]

export const ProductIntensity = [
  'Modérée',
  'Moyenne',
  'Élevée',
]

export const CannabinoidUnit = [
  '',
  '%',
  'mg/ml',
  'mg/unité',
]

export const ProductFormatUnit = [
  '',
  'g',
  'ml',
  'unité',
]
