
export interface ProductFormat {
  id: string
  quantity?: string
  price?: string
  unit?: string
}
