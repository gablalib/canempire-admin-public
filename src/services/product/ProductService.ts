import { Product } from '../../models/product/Product';
import { CanEmpireApi } from '../api/CanEmpireApi';
import axios from 'axios';

export interface ProductService {
  patch: (p: Product) => Promise<Response>
  getAll: () => Promise<Product[]>
  findByKeyword: (k: string) => Promise<Product[]>
}

export class ProductRestService extends CanEmpireApi implements ProductService {
  constructor() {
    super()
  }

  patch = (product: Product): Promise<Response> =>
    axios.put(`${this.productsPath}/${product.id}`, { product })

  getAll = (): Promise<Product[]> =>
    axios.get(this.productsPath)
      .then(response => response.data)

  findByKeyword = (keyword: string): Promise<Product[]> =>
    axios.post(this.findProductsPath, { filter:  { name: keyword } })
      .then(response => response.data)

}
