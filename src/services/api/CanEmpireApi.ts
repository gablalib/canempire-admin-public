
export class CanEmpireApi {
  private readonly baseUrl = 'http://localhost:8080'

  protected readonly productsPath = `${this.baseUrl}/products`
  protected readonly findProductsPath = `${this.baseUrl}/products/find`
  protected readonly reviewsPath = `${this.baseUrl}/reviews`
}
