import React, { Component, ReactNode } from 'react'
import { hot } from 'react-hot-loader'
import { Navbar, NavbarTab } from './navbar/Navbar'
import Content from './content/Content'
import './App.css'

interface AppState {
  currentTab: NavbarTab
}

class App extends Component<any, AppState> {

  constructor(props: any) {
    super(props)
    this.state = { currentTab: 'products' }
  }

  render(): ReactNode {
    return (
      <div className="app">
        <Navbar setCurrentTab={this.setCurrentTab}/>
        <Content tabDisplayed={this.state.currentTab}/>
      </div>
    )
  }

  setCurrentTab = (tab: NavbarTab) => {
    this.setState(() => ({ currentTab: tab }))
  }
}

export default hot(module)(App);
