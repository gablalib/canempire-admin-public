import React, { Component, ReactNode } from 'react'
import { NavbarTab } from '../navbar/Navbar'
import './Content.css'
import ProductsDisplay from './products/ProductsDisplay'
interface ContentProps {
  tabDisplayed: NavbarTab
}
export default class Content extends Component<ContentProps, any> {
  render() {
    return <div className='content-container container'>
      {this.displayContent()}
    </div>
  }

  displayContent(): ReactNode {
    switch (this.props.tabDisplayed) {
      case 'products':
        return <ProductsDisplay/>
      case 'reviews':
        return <div>reviews</div>
      default:
        return <p>Couldn't resolve tab to display</p>
    }
  }
}
