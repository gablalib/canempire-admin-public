import React, { Component } from 'react'
import { ProductIntensity } from '../../../../../models/product/Product'
import './ProductIntensityField.css'

interface ProductIntensityFieldProps {
  intensity: string
  onChange: (intensity: string) => void
}

interface ProductIntensityFieldState {
  intensity: string | null
}

export default class ProductIntensityField extends Component<ProductIntensityFieldProps, ProductIntensityFieldState> {

  state = {
    intensity: this.props.intensity
  }

  render() {
    return (
      <div className='content'>
        {ProductIntensity.map(intensity =>
          <button
            key={intensity}
            className={`intensity-button ${this.getActive(intensity) ? 'active-intensity' : ''} ${this.getBold(intensity)}`}
            onClick={() => this.handleClick(intensity)}
          >
            {intensity}
          </button>
        )}
      </div>
    )
  }

  private getActive(intensity: string): boolean {
    switch(this.state.intensity) {
      case 'Élevée': return true
      case 'Moyenne': return intensity === 'Modérée' || intensity === 'Moyenne'
      case 'Modérée': return intensity === 'Modérée'
      default: return false
    }
  }

  private getBold(intensity: string): 'bold-intensity' | '' {
    return intensity === this.state.intensity ? 'bold-intensity' : ''
  }

  private handleClick(intensity: string) {
    this.props.onChange(intensity);
    this.setState(() => ({ intensity: this.state.intensity === intensity ? null : intensity }))
  }
}
