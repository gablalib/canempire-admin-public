import React, { Component } from 'react';
import uuid from 'uuid';
import {
  CannabinoidUnit,
  Product,
  ProductCategory,
  ProductDominance,
  ProductFormatUnit,
  ProductType
} from '../../../../models/product/Product'
import './ProductForm.css'
import { ProductFormat } from '../../../../models/product/ProductFormat'
import ProductIntensityField from './fields/ProductIntensityField'
import { ProductService } from '../../../../services/product/ProductService';

interface ProductFormProps {
  product: Product
  productService: ProductService
}

interface ProductFormState {
  product: Product
}

export default class ProductForm extends Component<ProductFormProps, ProductFormState> {

  state = {
    product: this.props.product
  }

  render() {
    return (
      <div className='product-card columns is-multiline'>
        {this.nameField}
        {this.brandField}
        {this.descriptionField}
        {this.producerField}
        {this.categoryField}
        {this.typeField}
        {this.dominanceField}
        {this.intensityField}
        {this.thcField}
        {this.cbdField}
        {this.formatsField}
      </div>
    )
  }

  private readonly nameField =
    <div className='column is-half'>
      <input
        className='input main-input is-medium is-rounded'
        defaultValue={this.state.product.name}
        onChange={e => this.handleFieldChange('name', e.target.value)}
        onBlur={() => this.props.productService.patch(this.state.product)}
      />
      <p className='label is-pulled-left'>
        produit
      </p>
    </div>

  private readonly brandField =
    <div className='column is-half'>
      <input
        className='input is-medium is-rounded'
        defaultValue={this.state.product.brand}
        onChange={e => this.handleFieldChange('brand', e.target.value)}
        onBlur={() => this.props.productService.patch(this.state.product)}
      />
      <p className='label is-pulled-left'>
        marque
      </p>
    </div>

  private readonly descriptionField =
    <div className='column is-half'>
      <textarea
        className='textarea is-rounded'
        defaultValue={this.state.product.description}
        onChange={e => this.handleFieldChange('description', e.target.value)}
        onBlur={() => this.props.productService.patch(this.state.product)}
        rows={2}
      />
      <p className='label is-pulled-left'>
        description
      </p>
    </div>

  private readonly producerField =
    <div className='column is-half'>
      <input
        className='input is-rounded'
        defaultValue={this.state.product.producer}
        onChange={e => this.handleFieldChange('producer', e.target.value)}
        onBlur={() => this.props.productService.patch(this.state.product)}
      />
      <p className='label is-pulled-left'>
        producteur
      </p>
    </div>

  private readonly categoryField =
    <div className='column is-3'>
      <div className='type-select select is-rounded'>
        <select
          defaultValue={this.state.product.category}
          onChange={e => this.handleSelectChange('category', e.target.value)}
        >
          {ProductCategory.map(i =>
            <option key={i} value={i}>{i}</option>)}
        </select>
      </div>
      <p className='label is-pulled-left'>catégorie</p>
    </div>

  private readonly typeSelect =
    <div className='type-select select is-normal is-rounded'>
      <select
        defaultValue={this.state.product.type}
        onChange={e => this.handleSelectChange('type', e.target.value)}
      >
        {ProductType.map(i =>
          <option key={i} value={i}>{i}</option>)}
      </select>
    </div>

  private readonly typeField =
    <div className='column is-3'>
      {this.typeSelect}
      <p className='label is-pulled-left'>type</p>
    </div>

  private readonly dominanceField =
    <div className='column is-3'>
      <div className='type-select select is-rounded'>
        <select
          defaultValue={this.state.product.dominance}
          onChange={e => this.handleSelectChange('dominance', e.target.value)}
        >
          {ProductDominance.map(i =>
            <option key={i} value={i}>{i}</option>)}
        </select>
      </div>
      <p className='label is-pulled-left'>dominance</p>
    </div>

  private readonly intensityField =
    <div className='column is-3'>
      <ProductIntensityField
        intensity={this.state.product.intensity}
        onChange={intensity => this.handleIntensityChange(intensity)}
      />
      <p className='label is-pulled-left'>intensité</p>
    </div>

  private readonly thcField =
    <div className='column is-3'>
      <div className='range-input-container'>
        <input
          className='input is-rounded range-input'
          defaultValue={this.state.product.minThc}
          onChange={e => this.handleFieldChange('minThc', e.target.value)}
          onBlur={() => this.props.productService.patch(this.state.product)}
        />
        <input
          className='input is-rounded range-input'
          defaultValue={this.state.product.maxThc}
          onChange={e => this.handleFieldChange('maxThc', e.target.value)}
          onBlur={() => this.props.productService.patch(this.state.product)}
        />
        <div className='range-unit-select select is-normal is-rounded'>
          <select className='range-input'
            defaultValue={this.state.product.thcUnit}
            onChange={e => this.handleSelectChange('thcUnit', e.target.value)}
          >
            {CannabinoidUnit.map(i =>
              <option key={i} value={i}>{i}</option>)}
          </select>
        </div>
      </div>
      <p className='label is-pulled-left'>min THC  -  max THC</p>
    </div>

  private readonly cbdField =
    <div className='column is-3'>
      <div className='range-input-container'>
        <input
          className='input is-rounded range-input'
          defaultValue={this.state.product.minCbd}
          onChange={e => this.handleFieldChange('minCbd', e.target.value)}
          onBlur={() => this.props.productService.patch(this.state.product)}
        />
        <input
          className='input is-rounded range-input'
          defaultValue={this.state.product.maxCbd}
          onChange={e => this.handleFieldChange('maxCbd', e.target.value)}
          onBlur={() => this.props.productService.patch(this.state.product)}
        />
        <div className='range-unit-select select is-normal is-rounded'>
          <select className='range-input'
            defaultValue={this.state.product.cbdUnit}
            onChange={e => this.handleSelectChange('cbdUnit', e.target.value)}
          >
            {CannabinoidUnit.map(i =>
              <option key={i} value={i}>{i}</option>)}
          </select>
        </div>
      </div>
      <p className='label is-pulled-left'>min CBD  -  max CBD</p>
    </div>

  private readonly formatsField =
    <div className='column is-half'>
      {this.state.product.formats.map((format, index) =>
        <div key={format.id} style={{ display: 'flex' }}>
          <input
            className='input is-rounded'
            style={{ flex: 1 }}
            defaultValue={format?.quantity}
            onChange={e => this.handleFormatChange(index, 'quantity', e.target.value)}
          />
          <div className='select range-unit-select is-normal is-rounded' style={{ flex: 1, display: 'flex' }}>
            <select className='range-input' style={{ flex: 1 }}>
              {ProductFormatUnit.map(i =>
                <option key={i} value={i}>{i}</option>)}
            </select>
          </div>
          <div style={{ flex: 1, display: 'flex' }}>
            <input
              className='input is-rounded'
              style={{ flex: 3 }}
              defaultValue={format?.price}
              onChange={e => this.handleFormatChange(index, 'price', e.target.value)}
            />
            <div style={{ flex: 1 }} className='is-pulled-left'>$</div>
          </div>
        </div>
      )}
      <p className='label is-pulled-left'>Formats et prix</p>
    </div>

  private handleFieldChange(field: string, value: any) {
    this.setState(prev => ({ product: { ...prev.product, [field]: value } }))
  }

  private handleSelectChange(field: string, value: any) {
    this.setState(prev => ({ product: { ...prev.product, [field]: value } }),
      this.updateProduct
    );
  }

  private handleFormatChange(formatIndex: number, formatField: keyof ProductFormat, value: any) {
    const formats = [...this.state.product.formats]
    formats[formatIndex][formatField] = value
    this.setState(prev => ({ product: { ...prev.product, formats } }),
      this.updateProduct
    );
  }

  private handleIntensityChange(intensity: string) {
    this.setState(prev => ({ product: { ...prev.product, intensity } }),
      this.updateProduct
    );
  }

  private updateProduct() {
    this.props.productService.patch(this.state.product)
  }
}
