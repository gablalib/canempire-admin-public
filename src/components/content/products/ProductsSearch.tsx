import React, { Component } from 'react'
import './ProductsSearch.css'

interface ProductsSearchProps {
  searchKeyword: (keyword: string) => void
  isLoading: boolean
}

interface ProductsSearchState {
  inputValue: string
}

export default class ProductsSearch extends Component<ProductsSearchProps, ProductsSearchState> {

  state = {
    inputValue: ''
  }

  render() {
    const inputClass = `input is-large is-rounded ${this.props.isLoading ? 'is-loading' : ''}`
    return <div className='search-bar'>
      <input type='text'
             className={inputClass}
             placeholder='Rechercher un produit...'
             onChange={e => { console.log(e.target.value); this.setState({ inputValue: e.target.value }) }}
             onKeyPress={e => { if (e.key === 'Enter') { this.props.searchKeyword(this.state.inputValue) }}}
      />
    </div>
  }
}
