import React, { Component } from 'react'
import { Product } from '../../../models/product/Product'
import { ProductService } from '../../../services/product/ProductService'
import ProductForm from './card/ProductForm'
import './ProductsList.css'

interface ProductsListProps {
  products: Product[] | null
  productService: ProductService
}

export default class ProductsList extends Component<ProductsListProps, any> {
  render() {
    const productItems = this.props.products.map((p: Product) =>
      <div key={`product-${p.id}`}>
        <ProductForm product={p} productService={this.props.productService}/>
      </div>)

    return <div className='products-list'>
      {productItems}
    </div>
  }
}
