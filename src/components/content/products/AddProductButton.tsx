import React from 'react'
import './add-product-button.css'

interface AddProductButtonProps {
  onClick: () => void
}

export default class AddProductButton extends React.Component<AddProductButtonProps, any> {

  render = () =>
      <button
        className='button add-product-button'
        onClick={this.props.onClick}
      >
        <i className='material-icons add-product-button-icon'>add</i>
      </button>
}
