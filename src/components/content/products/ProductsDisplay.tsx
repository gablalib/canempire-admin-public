import React, { Component } from 'react'
import AddProductButton from './AddProductButton';
import ProductsSearch from './ProductsSearch'
import ProductsList from './ProductsList'
import './ProductsDisplay.css'
import { ProductRestService, ProductService } from '../../../services/product/ProductService'
import { Product } from '../../../models/product/Product'
import uuid from 'uuid'

interface ProductsDisplayProps {

}

interface ProductsDisplayState {
  products: Product[]
  isLoading?: boolean
}

export default class ProductsDisplay extends Component<ProductsDisplayProps, ProductsDisplayState> {

  state = {
    products: [] as Product[],
    isLoading: true
  }

  private productService: ProductService = new ProductRestService();

  componentDidMount() {
    this.productService.getAll()
      .then(products => {
        this.setState(() => ({ products, isLoading: false }))
      })
      .catch(() => this.setState({ isLoading: false }, () => console.log('No products found')))
  }

  render() {
    return <div className='products-display'>
      <ProductsSearch
        searchKeyword={keyword => this.loadProductsFromKeyword(keyword)}
        isLoading={this.state.isLoading}
      />
      <AddProductButton
        onClick={() => this.addProductCard()}
      />
      <div hidden={!this.state.isLoading}>
        Loading...
      </div>
      <ProductsList
        products={this.state.products}
        productService={this.productService}
      />
    </div>
  }

  private loadProductsFromKeyword(keyword: string) {
    this.setState({ isLoading: true }, () =>
      this.productService.findByKeyword(keyword)
        .then((products: Product[]) =>
          this.setState({ products, isLoading: false })))
  }

  private addProductCard() {
    this.setState((prev: ProductsDisplayState) => {
      const products: Product[] = [...prev.products];
      products.unshift({
        id: uuid.v4(),
        formats: [{ id: uuid.v4() }]
      })
      return ({ products });
    })
  }
}
