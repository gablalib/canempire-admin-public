import React, { Component } from 'react'
import NavbarLogo from './NavbarLogo'
import NavbarTabs from './NavbarTabs'

export type NavbarTab = 'products' | 'reviews'

interface NavbarProps {
  setCurrentTab: (tab: NavbarTab) => void
}

interface NavbarState {
  activeTab: NavbarTab
}

export class Navbar extends Component<NavbarProps, NavbarState> {
  render() {
    return (
      <div className='navbar is-fixed-top has-background-light'>
        <div className='navbar-brand'>
          <NavbarLogo/>
        </div>
        <div className='navbar-menu'>
          <NavbarTabs setTab={this.props.setCurrentTab}/>
        </div>
      </div>
    )
  }
}
