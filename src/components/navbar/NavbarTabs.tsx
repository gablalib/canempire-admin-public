import React, { Component } from 'react'
import './NavbarTabs.css'
import { NavbarTab } from './Navbar'

interface NavbarTabsProps {
  setTab: (tab: NavbarTab) => void
}

interface NavbarTabsState {
  activeTab: NavbarTab
}

export default class NavbarTabs extends Component<NavbarTabsProps, NavbarTabsState> {

  constructor(props: NavbarTabsProps) {
    super(props)
    this.state = { activeTab: 'products' }
  }

  render() {
    return (
      <div id='tabs' className='navbar-start navbar-tabs'>
        <div id='products-tab' className='navbar-tab active' onClick={() => this.setActiveTab('products')}>
          <div className='navbar-tab-title subtitle is-5'>
            PRODUITS SQDC
          </div>
        </div>
        <div id='reviews-tab' className='navbar-tab' onClick={() => this.setActiveTab('reviews')}>
          <div className='navbar-tab-title subtitle is-5'>
            REVUES APP MOBILE
          </div>
        </div>
      </div>
    )
  }

  setActiveTab(tabName: NavbarTab) {
    for (const tab of document.getElementById('tabs').children)
      tab.classList.remove('active')
    document.getElementById(`${tabName}-tab`).classList.add('active')
    this.props.setTab(tabName)
  }
}
