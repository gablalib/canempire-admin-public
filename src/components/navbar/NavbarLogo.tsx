import React, { Component } from 'react'
import canempireLogo from '../../assets/NOIR-GRIS-2-RGB.png';
import './NavbarLogo.css'

interface NavbarLogoProps {

}

interface NavbarLogoState {

}

export default class NavbarLogo extends Component<NavbarLogoProps, NavbarLogoState> {
  render() {
    return (
      <div className='navbar-logo'>
        <div className=' navbar-image'>
          <img
            src={canempireLogo}
            width={180}
            height={58}
          />
        </div>
        <h1 className='subtitle is-6'>
          ADMIN
        </h1>
      </div>
    )
  }
}
